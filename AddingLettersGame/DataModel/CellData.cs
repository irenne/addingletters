﻿namespace AddingLettersGame.DataModel
{
    public class CellData
    {
        public int Value { get; set; }

        public int Row { get; set; }

        public int Column { get; set; }
    }
}

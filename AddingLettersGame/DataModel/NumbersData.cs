﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddingLettersGame.DataModel
{
    public class NumbersData : INotifyPropertyChanged
    {
        const int rank = 4;
        private int[,] totalCells = new int[rank, rank];

        public NumbersData()
        {
            InitializeFirstTwoNumbers();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public List<CellData> Cells
        {
            get
            {
                var cells = totalCells.OfType<int>().Select(x => new CellData { Value = x});
                return cells.ToList();
            }
        }

        public void AddRandom()
        {
            AddRandomInEmptyCell();

            NotifyPropertyChanged("Cells");
        }

        public void MoveUp()
        {
            for (var c = 0; c < rank; c++)
            {
                MoveColumnUp(c);
                SumColumnUp(c);
                MoveColumnUp(c);
            }
            AddRandomInEmptyCell();

            NotifyPropertyChanged("Cells");
        }

        public void MoveDown()
        {
            for (var c = 0; c < rank; c++)
            {
                MoveColumnDown(c);
                SumColumnDown(c);
                MoveColumnDown(c);
            }
            AddRandomInEmptyCell();

            NotifyPropertyChanged("Cells");
        }

        public void MoveLeft()
        {
            for (var r = 0; r < rank; r++)
            {
                MoveRowLeft(r);
                SumRowLeft(r);
                MoveRowLeft(r);
            }
            AddRandomInEmptyCell();

            NotifyPropertyChanged("Cells");
        }

        public void MoveRight()
        {
            for (var r = 0; r < rank; r++)
            {
                MoveRowRight(r);
                SumRowRight(r);
                MoveRowRight(r);
            }
            AddRandomInEmptyCell();

            NotifyPropertyChanged("Cells");
        }

        private void InitializeFirstTwoNumbers()
        {
            var random = new Random();
            var randomRow = random.Next(rank);
            var randomColumn = random.Next(rank);

            totalCells[randomRow, randomColumn] = 2;

            randomRow = random.Next(rank);
            randomColumn = random.Next(rank);
            totalCells[randomRow, randomColumn] = 4;
        }

        private void MoveColumnUp(int colIndex)
        {
            var currentRow = 0;

            while (currentRow < rank)
            {
                if (totalCells[currentRow, colIndex] == 0)
                {
                    for (var i = currentRow; i < rank; i++)
                    {
                        if (totalCells[i, colIndex] > 0)
                        {
                            totalCells[currentRow, colIndex] = totalCells[i, colIndex];
                            totalCells[i, colIndex] = 0;
                            break;
                        }
                    }
                }
                currentRow++;
            }
        }

        private void MoveColumnDown(int colIndex)
        {
            var currentRow = rank - 1;

            while (currentRow >= 0)
            {
                if (totalCells[currentRow, colIndex] == 0)
                {
                    for (var i = currentRow; i >= 0; i--)
                    {
                        if (totalCells[i, colIndex] > 0)
                        {
                            totalCells[currentRow, colIndex] = totalCells[i, colIndex];
                            totalCells[i, colIndex] = 0;
                            break;
                        }
                    }
                }
                currentRow--;
            }
        }

        private void MoveRowLeft(int rowIndex)
        {
            var currentColumn = 0;

            while (currentColumn < rank)
            {
                if (totalCells[rowIndex, currentColumn] == 0)
                {
                    for (var i = currentColumn; i < rank; i++)
                    {
                        if (totalCells[rowIndex, i] > 0)
                        {
                            totalCells[rowIndex, currentColumn] = totalCells[rowIndex, i];
                            totalCells[rowIndex, i] = 0;
                            break;
                        }
                    }
                }
                currentColumn++;
            }
        }

        private void MoveRowRight(int rowIndex)
        {
            var currentColumn = rank - 1;

            while (currentColumn >= 0)
            {
                if (totalCells[rowIndex, currentColumn] == 0)
                {
                    for (var i = currentColumn; i >= 0; i--)
                    {
                        if (totalCells[rowIndex, i] > 0)
                        {
                            totalCells[rowIndex, currentColumn] = totalCells[rowIndex, i];
                            totalCells[rowIndex, i] = 0;
                            break;
                        }
                    }
                }
                currentColumn--;
            }
        }

        private void SumColumnUp(int colIndex)
        {
            for (var i = 0; i < rank - 1; i++)
            {
                if (totalCells[i, colIndex] == totalCells[i + 1, colIndex])
                {
                    totalCells[i, colIndex] *= 2;
                    totalCells[i + 1, colIndex] = 0;
                }
            }
        }

        private void SumColumnDown(int colIndex)
        {
            for (var i = rank-1; i > 0; i--)
            {
                if (totalCells[i, colIndex] == totalCells[i - 1, colIndex])
                {
                    totalCells[i, colIndex] *= 2;
                    totalCells[i - 1, colIndex] = 0;
                }
            }
        }

        private void SumRowLeft(int rowIndex)
        {
            for (var i = 0; i < rank - 1; i++)
            {
                if (totalCells[rowIndex, i] == totalCells[rowIndex, i + 1])
                {
                    totalCells[rowIndex, i] *= 2;
                    totalCells[rowIndex, i + 1] = 0;
                }
            }
        }

        private void SumRowRight(int rowIndex)
        {
            for (var i = rank - 1; i > 0; i--)
            {
                if (totalCells[rowIndex, i] == totalCells[rowIndex, i - 1])
                {
                    totalCells[rowIndex, i] *= 2;
                    totalCells[rowIndex, i - 1] = 0;
                }
            }
        }

        private void AddRandomInEmptyCell()
        {
            var emptyCell = GetRandomEmptyCell();
            if (emptyCell == null) return;

            totalCells[emptyCell.Row, emptyCell.Column] = 2;
        }

        private CellData GetRandomEmptyCell()
        {
            var emptyCells = GetEmptyCells();
            if (!emptyCells.Any()) return null;

            var random = new Random();
            var randomIndex = random.Next(emptyCells.Count);

            return emptyCells.ElementAt(randomIndex);
        }

        private List<CellData> GetEmptyCells()
        {
            var emptyCells = new List<CellData>();

            for (var i = 0; i < rank; i++)
            {
                for (var j = 0; j < rank; j++)
                {
                    if (totalCells[i, j] == 0)
                    {
                        emptyCells.Add(new CellData { Row = i, Column = j });
                    }
                }
            }

            return emptyCells;
        }

        private void NotifyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}

﻿using AddingLettersGame.DataModel;
using Windows.UI.Input;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace AddingLettersGame
{
    public sealed partial class NumbersGamePage : Page
    {
        NumbersData viewModel = null;

        public NumbersGamePage()
        {
            this.InitializeComponent();

            var gr = new GestureRecognizer();
           // TouchPanel.EnabledGestures = GestureType.Flick;
            //Microsoft.Phone.Controls.Toolkit
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            viewModel = new NumbersData();
            this.DataContext = viewModel;
        }

        private void BackButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Frame.Navigate(typeof(MainPage));
        }

        private void UpButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            viewModel.MoveUp();
        }

        private void DownButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            viewModel.MoveDown();
        }

        private void LeftButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            viewModel.MoveLeft();
        }

        private void RightButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            viewModel.MoveRight();
        }

        private void AddRandomButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            viewModel.AddRandom();
        }
    }
}
